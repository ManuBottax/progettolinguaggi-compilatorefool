package entry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import ast.FieldNode;
import ast.MethodNode;

public class CTentry implements Cloneable{
	
	private HashMap<String, STentry> vTable;
	private int offsetFields = -1 ; // diminuiscono
	private int offsetMethods = 0; //aumentano
	private ArrayList<FieldNode> allFields = new ArrayList<FieldNode>();
	private ArrayList<MethodNode> allMethods = new ArrayList<MethodNode>();
	private CTentry superClass = null;
	private HashSet<Integer> locals = new HashSet<>();
	
	public CTentry(HashMap<String, STentry> virtualTable){
		this.vTable = virtualTable;
	}
	
	public void setSuperClass(CTentry s){
		this.superClass = s;
		this.superClass.cloneFields(this.allFields);
		this.offsetFields = this.superClass.getFieldsOffset();
		this.superClass.cloneMethods(this.allMethods);
		this.offsetMethods = this.superClass.getMethodsOffset();
		this.superClass.cloneVTable(this.vTable);
	}
	
	public int getFieldsOffset() {
		return offsetFields;
	}
	
	public int getMethodsOffset() {
		return offsetMethods;
	}
	
	public ArrayList<FieldNode> getFields() {
		return allFields;
	}
	
	public ArrayList<MethodNode> getMethods() {
		return allMethods;
	}
	
	public HashSet<Integer> getLocals(){
		return this.locals;
	}
	
	public void addField(FieldNode field){
		if(vTable.containsKey(field.getID())) {
			if(!(vTable.get(field.getID()).isMethod())) {
				int index = -1;
				for(int i=0; i<allFields.size(); i++) {
					if(allFields.get(i).getID().equals(field.getID())) {
						index = i;
					}
				}
				allFields.remove(index);
				allFields.add(index, field);
				int offset = vTable.get(allFields.get(index).getID()).getOffset();
				if(locals.contains(offset)) {
					System.out.println("field " + field.getID() + " already declared");
					System.exit(0);
				}
				locals.add(offset);
				vTable.put(field.getID(), new STentry(1, field.getSymType(), offset));
			} else {
				System.out.println(field.getID() + " is already declared as a method");
				System.exit(0);
			}
		} else {
			if(locals.contains(offsetFields)) {
				System.out.println("field " + field.getID() + " already declared");
				System.exit(0);
			}
			locals.add(offsetFields);
			allFields.add(field);
			vTable.put(field.getID(), new STentry(1, field.getSymType(), offsetFields--));
		}
	}

	public STentry addMethod(MethodNode method){
		STentry entry = null;
		if(vTable.containsKey(method.getID())) {
			if(vTable.get(method.getID()).isMethod()) {
				int index = -1;
				for(int i=0; i<allMethods.size(); i++) {
					if(allMethods.get(i).getID().equals(method.getID())) {
						index = i;
					}
				}
				allMethods.remove(index);
				allMethods.add(index, method);
				int offset = vTable.get(allMethods.get(index).getID()).getOffset();
				if(locals.contains(offset)) {
					System.out.println("method " + method.getID() + " already declared");
					System.exit(0);
				}
				locals.add(offset);
				entry = new STentry(1, method.getSymType(), offset, true);
				vTable.put(method.getID(), entry);
			} else {
				System.out.println(method.getID() + " is already declared as a field");
				System.exit(0);
			}
		} else {
			if(locals.contains(offsetMethods)) {
				System.out.println("method " + method.getID() + " already declared");
				System.exit(0);
			}
			locals.add(offsetMethods);
			allMethods.add(method);
			entry = new STentry(1, method.getSymType(), offsetMethods++, true);
			vTable.put(method.getID(), entry);
		}
		return entry;
	}
	
	public HashMap<String, STentry> getVTable() {
		return vTable;
	}

	public STentry contains(String s) {
		return vTable.get(s);
	}
	
	public void cloneMethods(ArrayList<MethodNode> cloneMethods){
		try {
		    for(MethodNode meth: this.allMethods) {
		    	cloneMethods.add((MethodNode)meth.clone());
		    }
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
	
	public void cloneFields(ArrayList<FieldNode> cloneFields){
		try {
		    for(FieldNode fie: this.allFields) {
		    	cloneFields.add((FieldNode)fie.clone());
		    }
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
	
	public void cloneVTable(HashMap<String, STentry> vTableSub) {
		for(String key: vTable.keySet()) {
			vTableSub.put(key, vTable.get(key));
		}
	}
	
}
