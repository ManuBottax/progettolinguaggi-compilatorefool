package entry;

import ast.interfaces.Node;

public class STentry {
 
	private int nl;
	private Node type;
	private int offset;
	private boolean isMethod = false;
  
	public STentry(int nestingLevel, Node type, int offset, boolean isMethod) {
		this.nl = nestingLevel;
		this.type = type;
		this.offset = offset;
		this.isMethod = isMethod;
	}
  
	public STentry(int n, Node t, int os) {
		this(n, t, os, false);
	}

	public STentry(int n, int os) {
		this(n, null, os, false);
	}
  
	public void setIsMethod(final boolean isMethod) {
		this.isMethod = isMethod;
	}
   
	public boolean isMethod() {
		return this.isMethod;
	}
  
	public void addType(final Node type) {
		this.type = type;
	}
  
	public Node getType() {
		return type;
	}

	public int getOffset() {
		return offset;
	}
  
	public int getNestinglevel() {
		return nl;
	}
  
	public STentry copy(){
		return new STentry(nl, type, offset, isMethod);
	}
  
	public String toPrint(final String indent) { 
		return indent + "STentry: nestlev " + Integer.toString(nl) + "\n"
			   + indent + "STentry: type\n"
			   + type.toPrint(indent + "  ")
			   + indent + "STentry: offset " + Integer.toString(offset) + "\n";
	}
	
}  