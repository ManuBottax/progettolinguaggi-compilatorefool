package ast;
import java.util.ArrayList;
import java.util.List;

import ast.interfaces.Node;
import ast.type.ArrowTypeNode;
import entry.STentry;
import lib.FOOLlib;

public class CallNode implements Node {

	private String id;
	private STentry entry; 
	private ArrayList<Node> parlist;
	private int nestinglevel;

	public CallNode(String i, STentry e, ArrayList<Node> p, int nl) {
		id = i;
		entry = e;
		parlist = p;
		nestinglevel = nl;
	}
  
	public String toPrint(final String indent) {  
		String parlstr = "";
		for(Node par: parlist) {
			parlstr += par.toPrint(indent + "  ");
		}
		return indent + "Call:" + id + " at nestlev " + nestinglevel + "\n" 
	           + entry.toPrint(indent + "  ")
	           + parlstr;        
	}
	  
	public Node typeCheck () {                             
		ArrowTypeNode t = null;
	    if(entry.getType() instanceof ArrowTypeNode) {
	    	t = (ArrowTypeNode) entry.getType(); 
	    } else {
	    	System.out.println("Invocation of a non-function "+id);
	    	System.exit(0);
	    }
	    //controllo il numero dei parametri
	    List<Node> p = t.getParList();
	    if(p.size() != parlist.size()) {
	    	System.out.println("Wrong number of parameters in the invocation of "+id);
	    	System.exit(0);
	    } 
	    for(int i=0; i<parlist.size(); i++) {
	    	Node parType = (parlist.get(i)).typeCheck();
	    	Node decType = p.get(i);
	    	if((decType instanceof ArrowTypeNode && !(parType instanceof ArrowTypeNode)) || !(FOOLlib.isSubtype( parType, decType))) {
	    		System.out.println("Wrong type for " + (i+1) + "-th parameter in the invocation of " + id + " [used " + parType.toString() + " instead of " + decType.toString() + "]");
	    		System.exit(0);
	    	} 
	    }
	    return t.getRet();
	}
	  
	public String codeGeneration() {
		String parCode = ""; 
		for(int i=(parlist.size()-1); i>=0; i--) {
			parCode += parlist.get(i).codeGeneration();
		}
		//ora gestiamo il nesting e la risalita della catena statica
	    String getAR = "";
		for(int i=0; i<(nestinglevel-entry.getNestinglevel()); i++) { 
			getAR += "lw\n";
		}
		return "lfp" + "\n" //control link al chiamante
			   + parCode
			   //Recupera FP ad AR dichiarazione funzione (Per settare l'access link)
			   + "lfp\n"
			   + getAR
			   + "push " + entry.getOffset() + "\n"
			   + "add\n"
			   + "lw\n"
			   //Recupera indir funzione (Per saltare al codice della funzione)
			   + "lfp\n"
			   + getAR
			   + "push " + entry.getOffset() + "\n"
			   + "push 1\n"
			   + "sub\n"
			   + "add\n"
			   + "lw\n"
			   + "js" + "\n"; //salta a quell'indirizzo -> questo anche carica nel registro ra l'indirizzo dell'istruzione successiva quando torno indietro
		/// --- Fino a qua ho creato una parte dell'ar (quella del chiamante), fino al return address (che adesso � nel registro ar)
		/// --- la seconda met� la deve fare il chiamato --> FunNode
	}
	    
}