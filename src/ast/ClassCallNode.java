package ast;

import java.util.ArrayList;
import java.util.List;

import ast.interfaces.Node;
import ast.type.ArrowTypeNode;
import ast.type.ClassTypeNode;
import entry.STentry;
import lib.FOOLlib;

public class ClassCallNode implements Node{
	
	private String classID;
	private String methodID;
	private STentry classEntry;
	private STentry methodEntry;
	private ArrayList<Node> argList; 
	private int nestingLevel;
	
	public ClassCallNode(final String classID,final String methodID, final STentry entry, final STentry methodEntry, final ArrayList<Node> argList, final int nestingLevel) {
		this.classID = classID;
		this.methodID = methodID;
		this.classEntry = entry;
		this.methodEntry = methodEntry;
		this.argList = argList;
		this.nestingLevel = nestingLevel;
	}
	
	@Override
	public String toPrint(final String indent) {
		String parlstr = "";
		for(Node arg: argList) {
			parlstr += arg.toPrint(indent + "  ");
		}	
		return indent + "Method Call: " + classID + "." + methodID + " at nestlev " + nestingLevel + "\n" 
	           + classEntry.toPrint(indent + "  ") 
	           + methodEntry.toPrint(indent + "  ")
	           + parlstr;        
	}

	@Override
	public Node typeCheck() {
	     if(!(classEntry.getType() instanceof ClassTypeNode)) { 
	    	 System.out.println("Invocation of a method on a non-class: " + classID);
	    	 System.exit(0);
	     }
	     ArrowTypeNode m = null;
	     if(methodEntry.getType() instanceof ArrowTypeNode) {
	    	 m = (ArrowTypeNode) methodEntry.getType(); 
	     } else {
	    	 System.out.println("Invocation of a non-method: " + methodID);
	    	 System.exit(0);
	     }
	     List<Node> p = m.getParList();
	     if(!(p.size() == argList.size())) {
	    	 System.out.println("Wrong number of parameters in the invocation of " + methodID);
	    	 System.exit(0);
	     } 
	     for(int i=0; i<argList.size(); i++) {
	    	 Node parType = (argList.get(i)).typeCheck();
	    	 Node decType = p.get(i);
	    	 if(!(FOOLlib.isSubtype(parType, decType))) {
	    		 System.out.println("Wrong type for " + (i+1) + "-th parameter in the invocation of " + methodID + " [used " + parType.toString() + " instead of " + decType.toString() + "]");
	    		 System.exit(0);
	    	 } 
	     }
	     return m.getRet();
	}

	@Override
	public String codeGeneration() {
		String parCode = "";
		//Lista parametri dall'ultimo al primo
		for(int i=(argList.size()-1); i>=0; i--) {
			parCode += argList.get(i).codeGeneration();
		}
		//ora gestiamo il nesting e la risalita della catena statica verso la classe di riferimento
	    String getAR = "";
		for(int i=0; i<(nestingLevel-classEntry.getNestinglevel()); i++) { 
			getAR += "lw\n";
		}
		return  // setto un nuovo Access Link
				"lfp\n"
				+ parCode
				+ "lfp\n"
				+ getAR
				+ "push " + classEntry.getOffset() + "\n"
				+ "add\n"
				+ "lw\n"
				//sullo stack ho l'indirizzo dell'oggetto (obj pointer)
				+ "lfp\n"
				+ getAR
				+ "push " + classEntry.getOffset() + "\n"
				+ "add\n"
				+ "lw\n" 
				//Recupera indirizzo del metodo da chiamare
				+ "push " + methodEntry.getOffset() + "\n"
				+ "add\n"
				+ "lw\n"
				+ "js" + "\n" ;
	}

}
