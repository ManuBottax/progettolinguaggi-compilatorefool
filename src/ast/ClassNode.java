package ast;

import java.util.ArrayList;
import java.util.List;

import ast.interfaces.DecNode;
import ast.interfaces.Node;
import entry.CTentry;
import lib.FOOLlib;

public class ClassNode implements DecNode{
	
	private String id;
	private List<FieldNode> fields;
	private List<MethodNode> methods;
	private CTentry myCTentry;
	private CTentry superCTentry;
	
	public ClassNode(String id){
		this.id = id;
		this.fields = new ArrayList<>();
		this.methods = new ArrayList<>();
	}
	
	public void addCTentry(CTentry ct) {
		myCTentry = ct;
	}
	
	public void setSuperCTentry(CTentry s) {
		this.superCTentry = s;
		myCTentry.setSuperClass(superCTentry);
	}

	public void addField(FieldNode f){
		/*if(fields.stream().filter(field -> field.getID().equals(f.getID())).toArray().length != 0) {
			System.out.println("field " + f.getID() + " already declared in Class " + id);
			System.exit(0);
		}*/
		fields.add(f);
	}
	
	public void addMethod(MethodNode m){
		String metl = FOOLlib.freshMethLabel(); 
		m.setLabel(metl);
		methods.add(m);
	}	
	
	public String getId(){
		return this.id;
	}
	
	@Override
	public String toPrint(final String indent) {
		String fieldstr = "";
		for(FieldNode f: fields) {
			fieldstr += f.toPrint(indent + "  ");
		}
		String methstr = "";
		if(methods != null) 
			for(DecNode m: methods) {
				methstr += m.toPrint(indent + "  ");
			}
	    return indent + "Class:" + id + "\n"
			   + fieldstr  
		   	   + methstr; 
	}
	
	@Override
	public Node typeCheck() {
		if(methods != null) { 
			for (DecNode met: methods) {			
				met.typeCheck();
			}
		}
		if(superCTentry != null) {
			ArrayList<FieldNode> myField = myCTentry.getFields();
			ArrayList<FieldNode> superField = superCTentry.getFields();
			for(int i=0; i<superField.size(); i++) {
				if ( this.myCTentry.getLocals().contains(-1-i)){
					FOOLlib.isSubtype(myField.get(i).getSymType(), superField.get(i).getSymType());
				}
			}
			ArrayList<MethodNode> myMet = myCTentry.getMethods();
			ArrayList<MethodNode> superMet = superCTentry.getMethods();
			for(int i=0; i<superMet.size(); i++) {
				if ( this.myCTentry.getLocals().contains(i)){
					FOOLlib.isSubtype(myMet.get(i).getSymType(), superMet.get(i).getSymType());
				}
			}
		}
		return null;
	}

	@Override
	public String codeGeneration() {
		for(MethodNode m: methods){
			m.codeGeneration();
		}
		return "";
	}

	@Override
	public Node getSymType() {
		return null;
	}

}
