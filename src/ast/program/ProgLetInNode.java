package ast.program;
import java.util.ArrayList;

import ast.ClassNode;
import ast.interfaces.DecNode;
import ast.interfaces.Node;
import lib.*;

public class ProgLetInNode implements Node {

	private ArrayList<ClassNode> classList; 
	private ArrayList<DecNode> declist;
	private Node exp;
  
	public ProgLetInNode(ArrayList<ClassNode> cllist, ArrayList<DecNode> d, Node e) {
		classList = cllist;
		declist = d;
		exp = e;
	}
  
	public String toPrint(final String indent) {
		String clstr = "";
		String declstr = "";
		for(ClassNode cl: classList) {
			clstr += cl.toPrint(indent + "  ");
		}
		for(DecNode dec: declist) {
			declstr += dec.toPrint(indent + "  ");
		}
		return indent + "ProgLetIn\n" + clstr 
			   + declstr + exp.toPrint(indent + "  "); 
	}
  
	public Node typeCheck () {
		for(ClassNode cl: classList) {
			cl.typeCheck();  
		}
		for(DecNode dec: declist) {
			dec.typeCheck();
		}
		return exp.typeCheck();
	}
  
	public String codeGeneration() {
		String decListCode = "";
		String clListCode = "";
		for(ClassNode cl: classList) {
			clListCode += cl.codeGeneration();
		}
		for(DecNode dec: declist) {
			decListCode += dec.codeGeneration();
		}
		return "push 0\n"
			   + clListCode 
	  		   + decListCode
	  		   + exp.codeGeneration()
	  		   + "halt\n"
	  		   + FOOLlib.getCode();
	} 
    
}