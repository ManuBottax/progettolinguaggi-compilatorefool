package ast.program;

import ast.interfaces.Node;

public class ProgNode implements Node {

	private Node exp;
  
	public ProgNode(Node e) {
		exp = e;
	}
  
	public String toPrint(final String indent) {
		return "Prog\n" + exp.toPrint("  ") ;
	}
  
	public Node typeCheck() {
		return exp.typeCheck();
	}  
  
	public String codeGeneration() {
		return exp.codeGeneration() + "halt\n";
	}  

}  