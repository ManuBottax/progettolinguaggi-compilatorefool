package ast;

import java.util.ArrayList;
import java.util.List;

import ast.interfaces.Node;
import ast.type.ClassTypeNode;
import entry.CTentry;
import lib.FOOLlib;

public class NewNode implements Node{
	
	private CTentry entry;
	private CTentry effectiveType;
	private String id;
	private List<Node> fields;
	
	public NewNode(CTentry e, String id) {
		this.entry = e;
		this.effectiveType = e;
		this.id = id;
		this.fields = new ArrayList<>();
	}
	
	public void setEffectiveType(CTentry type) {
		this.effectiveType = type;
	}
	
	public void addParameter(Node par){
		this.fields.add(par);
	}

	@Override
	public String toPrint(final String indent) {
		return indent + "New Object of class " + id + "\n";
	}

	@Override
	public Node typeCheck() {
		if(entry.getFields().size() != fields.size()) {
			System.out.println("Wrong number of parameters in new call of " + id);
    		System.exit(0);
		}
	    for(int i=0; i<entry.getFields().size(); i++) {
	    	if(!FOOLlib.isSubtype(fields.get(i).typeCheck(), entry.getFields().get(i).getSymType())) {
	    		System.out.println("Wrong type for " + (i+1) + "-th parameter in the invocation of " + id);
	    		System.exit(0);
	    	}
	    }
		return new ClassTypeNode(id);
	}

	@Override
	public String codeGeneration() {
		ArrayList<FieldNode> allFields = new ArrayList<FieldNode>();
		ArrayList<MethodNode> allMethods = new ArrayList<MethodNode>();
		String parCode = "";
		for(Node n: fields) {
			parCode += n.codeGeneration();
		}
		// bisogna capire come gestire i campi ereditati (non so se tutti i campi hanno un valore come parametro)
		String fieldsCreation = "";
		entry.cloneFields(allFields);
		for(int i=0; i<allFields.size(); i++) {
			String s = allFields.get(i).getID();
			if(effectiveType.contains(s) != null) {
				fieldsCreation += "lhp\n" +
						  		  "sw\n" + 
						  		  "lhp\n" + 
						  		  "push 1\n" +
						  		  "add\n" + 
						  		  "shp\n";
			}
		}   
		String methCreation = "";
		entry.cloneMethods(allMethods);
		if(allMethods.isEmpty()) {
			methCreation += "lhp\n" +
					        "push 1\n" +
					        "add\n" + 
					        "shp\n"; 
		} else {
			for(MethodNode m : allMethods ) {
				if(effectiveType.contains(m.getID()) != null) {
					methCreation += "push " + m.getLabel() + "\n" + 
						         	"lhp\n" +
						         	"sw\n" + 
						          	"lhp\n" + 
						         	"push 1\n" +
						           	"add\n" + 
						         	"shp\n";
				}
			} 
		}
		return parCode
			   + fieldsCreation
			   + "lhp\n"
			   + methCreation;
			   // ho il valore dell'object pointer sullo stack
	}

}