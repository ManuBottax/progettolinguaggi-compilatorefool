package ast;

import ast.interfaces.DecNode;
import ast.interfaces.Node;
import lib.FOOLlib;

public class VarNode implements Node, DecNode {

	private String id;
	private Node type;
	private Node exp;
  
	public VarNode(String i, Node t, Node v) {
		id = i;
		type = t;
		exp = v;
	}
  
	@Override
	public String toPrint(final String indent) {
		return indent + "Var: " + id + "\n"
			   + type.toPrint(indent + "  ")  
			   + exp.toPrint(indent + "  "); 
	}
  
	@Override
	public Node typeCheck () {
		if(!(FOOLlib.isSubtype(exp.typeCheck(),type))) { 
			System.out.println("incompatible value for variable "+id);
			System.exit(0);
		}     
		return null;
	}
  
	@Override
	public String codeGeneration() {
		return exp.codeGeneration();
	}
  
	@Override
	public Node getSymType() {
		return type;
	}
    
}