package ast;

import ast.interfaces.DecNode;
import ast.interfaces.Node;

public class FieldNode implements DecNode, Cloneable{

	private String id;
	private Node type;
	  
	public FieldNode (String id, Node type) {
		this.id = id;
		this.type = type;
	}
	  
	@Override
	public String toPrint(final String indent) {
		return indent + "Field: " + id + "\n"
			   + type.toPrint(indent + "  "); 
	}
	  
	@Override
	public Node typeCheck() {
		return null;
	}
	  
	@Override
	public String codeGeneration() {
		return "";
	}
	  
	public String getID() {
		return this.id;
	}
	  
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
	
	@Override
	public Node getSymType() {
		return type;
	}

}