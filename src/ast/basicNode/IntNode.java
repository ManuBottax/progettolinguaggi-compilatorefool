package ast.basicNode;

import ast.interfaces.Node;
import ast.type.IntTypeNode;

public class IntNode implements Node {

	private Integer val;
  
	public IntNode(Integer n) {
		val = n;
	}
  
	public String toPrint(final String indent) {
		return indent + "Int:" + Integer.toString(val) + "\n";  
	}
  
	public Node typeCheck() {
		return new IntTypeNode();
	} 
  
	public String codeGeneration() {
		return "push " + val + "\n";
	}

} 