package ast.basicNode;

import ast.interfaces.Node;
import ast.type.BoolTypeNode;

public class BoolNode implements Node {

	private boolean val;
  
	public BoolNode(boolean n) {
		val = n;
	}
  
	public String toPrint(final String indent) {
		if(val) {
			return indent + "Bool:true\n";
		} else {
			return indent + "Bool:false\n";  
		}
	}
  
	public Node typeCheck() {
		return new BoolTypeNode();
	}    
  
	public String codeGeneration() {
		return "push " + (val? 1 : 0) + "\n";
	}
     
}