package ast.basicNode;

import ast.interfaces.Node;
import ast.type.EmptyTypeNode;

public class EmptyNode implements Node{

	@Override
	public String toPrint(final String indent) {
		return indent + "Null\n";
	}

	@Override
	public Node typeCheck() {
		return new EmptyTypeNode();
	}

	@Override
	public String codeGeneration() {
		return "push -1" + "\n";
	}

}
