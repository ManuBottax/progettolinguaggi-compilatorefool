package ast;

import ast.interfaces.Node;
import ast.type.BoolTypeNode;
import lib.FOOLlib;

public class IfNode implements Node {

	private Node cond;
	private Node th;
	private Node el;
  
	public IfNode (Node c, Node t, Node e) {
		cond = c;
		th = t;
		el = e;
	}
  
	public String toPrint(final String indent) {
		return indent + "If\n" + cond.toPrint(indent + "  ") 
               + th.toPrint(indent + "  ")   
               + el.toPrint(indent + "  "); 
	}
  
	public Node typeCheck() {
		if(!(FOOLlib.isSubtype(cond.typeCheck(),new BoolTypeNode()))) {
			System.out.println("non boolean condition in if");
			System.exit(0);
		}
		Node t = th.typeCheck();
		Node e = el.typeCheck();
		Node lca = FOOLlib.lowestCommonAncestor(t, e);
		if(lca != null) {
			return lca; 
		} else {
			System.out.println("Incompatible types in then else branches");
			System.exit(0);
			return null;
		}
		//Versione base senza LowestCommonAncestor
		/*
		if(FOOLlib.isSubtype(t,e)) { 
			return e;
		}
		if(FOOLlib.isSubtype(e,t)) {
			return t;
		}
		System.out.println("Incompatible types in then else branches");
		System.exit(0);
		return null;*/
	}
  
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel(); 
		String l2 = FOOLlib.freshLabel();
		return cond.codeGeneration()
			   + "push 1\n"
			   + "beq " + l1 + "\n"
			   + el.codeGeneration()
			   + "b " + l2 + "\n"
			   + l1 + ":\n"
			   + th.codeGeneration() 
			   + l2 + ":\n"; 
	}

}