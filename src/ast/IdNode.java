package ast;

import ast.interfaces.Node;
import ast.type.ArrowTypeNode;
import entry.STentry;

public class IdNode implements Node {

	private String id;
	private STentry entry;
	private int nestinglevel;
  
	public IdNode(String i, STentry st, int nl) {
		id = i;
		entry = st;
		nestinglevel = nl;
	}
  
	public String toPrint(final String indent) {
		return indent + "Id:" + id + " at nestlev " + nestinglevel + "\n" + entry.toPrint(indent + "  ") ;  
	}
  
	public Node typeCheck () {
		if(entry.getType() == null) {
			System.out.println("A class cannot be an id");
			System.out.println(0);
		}
		if(entry.isMethod()) {
			System.out.println("A method cannot be an id");
			System.out.println(0);
		}
		return entry.getType();
	}
  
	public String codeGeneration() {
		String getAR = "";
		for(int i=0; i<(nestinglevel-entry.getNestinglevel()); i++) { 
			getAR += "lw\n";
		}
		//calcola indirizzo offset+$fp
		String code = "lfp\n" + getAR //risalgo la catena statica
					  + "push " + entry.getOffset() + "\n"
					  + "add\n"
					  + "lw\n";
        if(entry.getType() instanceof ArrowTypeNode) {
        	code += "lfp\n" + getAR //risalgo la catena statica
        			+ "push " + entry.getOffset() + "\n" //calcola indirizzo offset+$fp
        			+ "push 1\n"
        			+ "sub\n"
        			+ "add\n"
        			+ "lw\n"; //carica sullo stack il valore a quell'indirizzo
        }
	    return code;
	}

}