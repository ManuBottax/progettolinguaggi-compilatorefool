package ast;

import ast.interfaces.DecNode;
import ast.interfaces.Node;

public class ParNode implements Node, DecNode {

	private String id;
	private Node type;
  
	public ParNode(String i, Node t) {
		id = i;
		type = t;
	}
  
	@Override
	public String toPrint(final String indent) {
		return indent + "Par:" + id + "\n"
			   + type.toPrint(indent + "  ") ; 
	}
  
	@Override
	public Node typeCheck () {
		return null;
	}
	
	@Override
	public String codeGeneration() {
		return "";
	}

	@Override
	public Node getSymType() {
		return type;
	}

}