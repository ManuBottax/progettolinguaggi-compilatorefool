package ast.basicOperator;

import ast.interfaces.Node;

public class PrintNode implements Node {

	private Node val;
  
	public PrintNode(Node v) {
		val = v;
	}
  
	public String toPrint(final String indent) {
		return indent + "Print\n" + val.toPrint(indent + "  ") ;
	}
  
	public Node typeCheck() {
		return val.typeCheck();
	}  
  
	public String codeGeneration() {
		return val.codeGeneration() + "print\n";
	}

}