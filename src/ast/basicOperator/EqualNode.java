package ast.basicOperator;

import ast.interfaces.Node;
import ast.type.ArrowTypeNode;
import ast.type.BoolTypeNode;
import lib.FOOLlib;

public class EqualNode implements Node {

	private Node left;
	private Node right;
  
	public EqualNode(Node l, Node r) {
		left = l;
		right = r;
	}
  
	public String toPrint(final String indent) {
		return indent + "Equal\n" + left.toPrint(indent + "  ")   
               + right.toPrint(indent + "  ") ; 
	}
  
	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		if(l instanceof ArrowTypeNode || r instanceof ArrowTypeNode) {
			System.out.println("Functional types not allowed with equals operator.");
			System.exit(0);
		}
		if(!(FOOLlib.isSubtype(l,r) || FOOLlib.isSubtype(r,l))) {
			System.out.println("Incompatible types in equal");
			System.exit(0);
		}
		return new BoolTypeNode();
	}  
  
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel(); 
		String l2 = FOOLlib.freshLabel();
		return left.codeGeneration()
			   + right.codeGeneration()
			   + "beq "+ l1 +"\n"
			   + "push 0\n"
			   + "b " + l2 + "\n"
			   + l1 + ":\n"
			   + "push 1\n"
			   + l2 + ":\n";
	}

}  