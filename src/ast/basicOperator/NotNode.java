package ast.basicOperator;

import ast.interfaces.Node;
import ast.type.BoolTypeNode;
import lib.FOOLlib;

public class NotNode implements Node {

	private Node argument;
  
	public NotNode(Node a) {
		argument = a;
	}
  
	public String toPrint(final String indent) {
		return indent + "Not\n" + argument.toPrint(indent + "  "); 
	}
  
	public Node typeCheck() {
		if(!(FOOLlib.isSubtype(argument.typeCheck(),new BoolTypeNode()))) {
			System.out.println("Non Boolean in not");
			System.exit(0);
		}
		return new BoolTypeNode();
	}  
  
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel(); 
	    String l2 = FOOLlib.freshLabel();
		return argument.codeGeneration()
			   + "push 0\n"
			   + "beq " + l1 + "\n"
			   + "push 0\n"
			   + "b " + l2 + "\n"
			   + l1 + ":\n"
			   + "push 1\n"
			   + l2 + ":\n";
	}

}