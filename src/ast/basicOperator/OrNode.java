package ast.basicOperator;

import ast.interfaces.Node;
import ast.type.BoolTypeNode;
import lib.FOOLlib;

public class OrNode implements Node {

	private Node left;
	private Node right;
  
	public OrNode(Node l, Node r) {
		left = l;
		right = r;
	}
  
	public String toPrint(final String indent) {
		return indent + "Or\n" + left.toPrint(indent + "  ")  
               + right.toPrint(indent + "  ") ; 
	}
  
	public Node typeCheck() {
		if(!(FOOLlib.isSubtype(left.typeCheck(),new BoolTypeNode()) && FOOLlib.isSubtype(right.typeCheck(),new BoolTypeNode()))) {
			System.out.println("Non Boolean in Or");
			System.exit(0);
		}
		return new BoolTypeNode();
	}
  
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel(); 
		String l2 = FOOLlib.freshLabel();
		String l3 = FOOLlib.freshLabel(); 
		return left.codeGeneration()
			   + "push 0\n"
				+ "beq " + l1 + "\n" //in case the first is false then goes to check the second
				+ "push 1\n"
				+ "b " + l3 + "\n" //in case the first is true returns 1
				+ l1 + ":\n"
			    + right.codeGeneration()
				+ "push 0\n" //check the second
				+ "beq " + l2 + "\n"
				+ "push 1\n" //returns 1 in case is true, 0 in case is false
				+ "b " + l3 + "\n"
				+ l2 + ":\n"
				+ "push 0\n"
			    + l3 + ":\n";
	}
  
}