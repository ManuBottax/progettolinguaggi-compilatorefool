package ast.basicOperator;

import ast.interfaces.Node;
import ast.type.BoolTypeNode;
import lib.FOOLlib;

public class GreaterEqualNode implements Node {

	private Node left;
	private Node right;
  
	public GreaterEqualNode(Node l, Node r) {
		left = l;
		right = r;
	}
  
	public String toPrint(final String indent) {
		return indent + "GreaterEqual\n" + left.toPrint(indent + "  ")   
               + right.toPrint(indent + "  ") ; 
	}
  
	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		if(!(FOOLlib.isSubtype(l,r) || FOOLlib.isSubtype(r,l))) {
			System.out.println("Incompatible types in greater equal");
			System.exit(0);
		}
		return new BoolTypeNode();
	}  
  
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel(); 
		String l2 = FOOLlib.freshLabel();
		return right.codeGeneration()
			   + left.codeGeneration() //equal to LowerEqualNode but with inverted arguments 
			   + "bleq "+ l1 +"\n"
			   + "push 0\n"
			   + "b " + l2 + "\n"
			   + l1 + ":\n"
			   + "push 1\n"
			   + l2 + ":\n";
  }

}  