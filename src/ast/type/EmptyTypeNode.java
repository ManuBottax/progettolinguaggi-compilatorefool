package ast.type;

import ast.interfaces.Node;

public class EmptyTypeNode implements Node {

	@Override
	public String toPrint(final String indent) {
		return indent + "NullType\n";
	}

	@Override
	public Node typeCheck() {
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}

}
