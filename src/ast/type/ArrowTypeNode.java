package ast.type;

import java.util.List;

import ast.interfaces.Node;

public class ArrowTypeNode implements Node {

	private List<Node> parlist; 
	private Node ret;
  
	public ArrowTypeNode(final List<Node> p, final Node r) {
		parlist = p;
		ret = r;
	}
    
	public String toPrint(final String indent) { 
		String parlstr = "";
		for(Node par: parlist) {
			parlstr += par.toPrint(indent + "  ");
		}
		return indent + "ArrowType\n" + parlstr + ret.toPrint(indent + "  ->") ; 
	}
  
	public Node getRet() { 
		return ret;
	}
  
	public List<Node> getParList() { 
		return parlist;
	}

	public Node typeCheck() {
		return this;
	}

	public String codeGeneration() {
		return "";
	}
  
	@Override
	public String toString() {
		return " ArrowType ";
	}

}