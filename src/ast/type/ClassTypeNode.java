package ast.type;

import ast.interfaces.Node;

public class ClassTypeNode implements Node{

	private String id;
	
	public ClassTypeNode(final String id) {
		this.id = id;
	}
	
	public String getId(){
		return id;
	}
	
	public String toPrint(final String indent) {
		return indent + "ClassType\n";  
	}

	public Node typeCheck() {
	    return null;
	}

	public String codeGeneration() {
		return "";
	}
		  
	@Override
	public String toString(){
		return " ClassType ";
	}

}
