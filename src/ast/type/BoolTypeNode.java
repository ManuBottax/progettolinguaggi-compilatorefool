package ast.type;

import ast.interfaces.Node;

public class BoolTypeNode implements Node {
  
	public String toPrint(final String indent) {
		return indent + "BoolType\n";  
	}
    
	public Node typeCheck() {
		return null;
	}
  
	public String codeGeneration() {
		return "";
	}
  
	@Override
	public String toString(){
		return " BoolType ";
	}
    
}