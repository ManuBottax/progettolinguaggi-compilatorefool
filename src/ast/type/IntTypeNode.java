package ast.type;

import ast.interfaces.Node;

public class IntTypeNode implements Node {
  
	public String toPrint(final String indent) {
		return indent + "IntType\n";  
	}
  
	public Node typeCheck() {
		return null;
	}

	public String codeGeneration() {
		return "";
	}
  
	@Override
	public String toString(){
		return " IntType ";
	}
  
}