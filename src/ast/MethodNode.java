package ast;

import java.util.ArrayList;

import ast.interfaces.DecNode;
import ast.interfaces.Node;
import ast.type.ArrowTypeNode;
import lib.FOOLlib;

public class MethodNode implements DecNode, Cloneable {
	
	private String id;
	private String label;
	private ArrayList<ParNode> parlist = new ArrayList<ParNode>(); 
	private ArrayList<DecNode> declist; 
	private Node body;
	private Node retType;
	private Node metType;
	 
	public MethodNode(String id, Node type) {
		this.id = id;
		this.retType = type;
		this.metType = new ArrowTypeNode(new ArrayList<>(), retType);
	}
	
	public String getID() {
		return id;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public ArrayList<ParNode> getParList() {
		return parlist;
	}
	
	public void setLabel(String l) {
		label = l;
	}
	  
	public void addDec(ArrayList<DecNode> d) {
	    declist = d;
	}
	  
	public void addBody(Node b) {
		body = b;
	}
		  
	public void addPar(ParNode p) {
		parlist.add(p);
	}  
		  
	@Override
	public String toPrint(final String indent) {
		String parlstr = "";
		for(ParNode par: parlist) {
			parlstr += par.toPrint(indent+"  ");
		}
		String declstr = "";
		if (declist != null) 
			for(DecNode dec: declist) {
				declstr += dec.toPrint(indent+"  ");
			}
	    return indent + "Method:" + id + "\n"
			   + retType.toPrint(indent + "  ")
			   + parlstr
		   	   + declstr
	           + body.toPrint(indent + "  ") ; 
	}

	@Override
	public Node typeCheck() {
		if(declist != null) {
			for(DecNode dec: declist) {
				dec.typeCheck();
			 }
		}
		if(!(FOOLlib.isSubtype(body.typeCheck(),retType))) {
			System.out.println("Wrong return type for function "+id);
		    System.exit(0);
		}  
		return null;
	}
 
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
	
	@Override
	public String codeGeneration() {		  
		String decListCode = "";
		if(declist != null) {
			for(Node dec:declist) {
				decListCode += dec.codeGeneration();
		  	}
		}
		//faccio una pop per ogni dichiarazione locale <-- dovr� distruggere lo stack dopo la chiamata
		String popDecl = "";
		if(declist != null) {
			for(DecNode dec: declist) {
				if (dec.getSymType() instanceof ArrowTypeNode) {
					popDecl += "pop\n";
		  		}
				popDecl += "pop\n";
		  	}
		}
		//faccio una pop per ogni parametro <-- dovr� distruggere lo stack dopo la chiamata
		String popParl = "";
		if(parlist != null) {
			for(ParNode par: parlist) {
				if (par.getSymType() instanceof ArrowTypeNode) {
					popParl += "pop\n";
		  		}
		  		popParl += "pop\n";
		  	}
		}
		FOOLlib.putCode(label + ":\n"
						+ "cfp" + "\n" // setto il fp all'indirizzo dell'oggetto sullo heap e butto via l'indirizzo dallo stack
		                + "lra" + "\n" // inserisce nello stack il return address
		                + decListCode // -- fine AR 
		                + body.codeGeneration() //codice della funzione
		                // adesso ho finito la mia funzione, devo ripulire lo stack dall'ar per mantenere l'invariante -> smonto lo stack pezzo per pezzo
		                + "srv" + "\n" //salvo in un registro il return value della funzione ( servirà alla fine)
		                + popDecl //pulisco lo stack dalle chiamate locali ( 1 pop per ogni variabile)
		                + "sra" + "\n" // pop del return address (salvandolo in ra)
		                + "pop\n" //libera l'access link
		                + popParl //pulisco lo stack dai parametri ( 1 pop per ogni parametro)
		                + "sfp" + "\n" // setto $fp al valore del control link
		                + "lrv" + "\n" // metto in cima allo stack il risultato della funzione
		                + "lra" + "\n" // metto l'indirizzo a cui saltare sullo stack ( per un attimo solo) 
		                + "js"  + "\n" 			// salta all'indirizzo che ho appena messo ( e lo poppa ) 
		    		);
		    // la chiamata di un metodo � una qualsiasi espressione, quindi deve valere l'invariante
			return "lfp\n" + "push "+ label +"\n";
	}

	@Override
	public Node getSymType() {
		return metType;
	}

}
