package lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ast.interfaces.Node;
import ast.type.ArrowTypeNode;
import ast.type.BoolTypeNode;
import ast.type.ClassTypeNode;
import ast.type.EmptyTypeNode;
import ast.type.IntTypeNode;

public class FOOLlib {
  
	private static int labCount = 0; 
	private static int funLabCount = 0; 
	private static int classLabCount = 0; 
	private static int methLabCount = 0;   
	private static String funCode = "";
	private static HashMap<String, String> superTypes;
	
	public static void setSuperType(HashMap<String, String> map) {
		superTypes = map;
	}
  
	//controllo che superCl sia sopratipo di cl
	private static boolean isSuperType(String superCl, String cl){
		if(superTypes.get(cl) != null) {
			if( superTypes.get(cl).equals(superCl)){
				return true;
			} else {
				return isSuperType(superCl, superTypes.get(cl));
			}
		}
		return false;
	}
	
	public static Node lowestCommonAncestor(Node a, Node b) {
		//// Controllo tra Classi ////	
		if(a instanceof EmptyTypeNode && b instanceof ClassTypeNode) {
			return b;
		} else if(b instanceof EmptyTypeNode && a instanceof ClassTypeNode) {
			return a;
		}
		if(a instanceof ClassTypeNode && b instanceof ClassTypeNode) {
			Node sa = new ClassTypeNode(superTypes.get(a));		
			if(!isSubtype(b,a)) {
				if(((ClassTypeNode)sa).getId() == null) {
					return null;
				}
				lowestCommonAncestor(sa, b);
			}
			return a;
		}
	    //// Controllo tra funzioni ////
		if(a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			ArrowTypeNode fa = (ArrowTypeNode) a;
			ArrowTypeNode fb = (ArrowTypeNode) b;
			List<Node> parA = fa.getParList();
			List<Node> parB = fb.getParList();
			if(parA.size() != parB.size()) {
				return null;
			}
			Node ret = lowestCommonAncestor(fa.getRet(), fb.getRet());
			if(ret != null){
					ArrayList<Node> parlist = new ArrayList<Node>();
					//Controllo sul tipo dei parametri
					for(int i=0; i<parA.size(); i++) {
						if(isSubtype(parA.get(i),parB.get(i))) {
							parlist.add(parA.get(i));
						} else {
							if(isSubtype(parB.get(i),parA.get(i))) {
								parlist.add(parB.get(i));
							} else {
								return null;
							}
						}
					}
					return new ArrowTypeNode(parlist, ret);
			}
		}
		//// Controllo tra tipi primitivi ////		
	    //controllo il caso bool-int ( ritorno gli interi )
		if(a instanceof BoolTypeNode && b instanceof IntTypeNode) {
			return b;
		}
		if(a instanceof IntTypeNode && b instanceof BoolTypeNode) {
			return a;
		}
		//Se sono della stessa classe ne ritorna uno dei due (int-int o bool-bool)
		if (a.getClass().equals(b.getClass())) {
			return a;
		}
		return null;
	}

	// controlla che a sia sottotipo di b
	public static boolean isSubtype (Node a, Node b) {
		Node fa = null;
		Node fb = null;
		boolean parTypes = true;
		if(a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			parTypes = isParameterSupertype(((ArrowTypeNode) a).getParList(), ((ArrowTypeNode) b).getParList());
		}
		if(a instanceof ClassTypeNode && b instanceof ClassTypeNode) {
			if(((ClassTypeNode) a).getId().equals(((ClassTypeNode) b).getId())) {
				return true;
			} else {
				return isSuperType(((ClassTypeNode) b).getId(), ((ClassTypeNode) a).getId());
			}
		}
		if(a instanceof ArrowTypeNode) { 
			fa = ((ArrowTypeNode) a).getRet();
		} else { 
			fa = a;
		}
		if(b instanceof ArrowTypeNode) { 
			fb = ((ArrowTypeNode) b).getRet();
		} else { 
			fb = b;
		}
		boolean retType = fa.getClass().equals(fb.getClass()) ||
    				   		((fa instanceof BoolTypeNode) && (fb instanceof IntTypeNode)) ||
    				   		((fa instanceof EmptyTypeNode) && (fb instanceof ClassTypeNode));
		boolean res = retType && parTypes;
		return res;
	} 
		
	//Controlla la controvarianza dei parametri dei ArrowType -> devono essere super tipi per poter fare override di metodi con tipi di ritorno/parametri diversi (come vogliamo noi)
	private static boolean isParameterSupertype(List<Node> aPar, List<Node> bPar){
		if(aPar.size() != bPar.size()) {
			return false;
		}	
		for(int i = 0; i<aPar.size(); i++) {
			if(!isSubtype(bPar.get(i), aPar.get(i))) {
				return false;
			}
		}
		return true;
	}
	
	public static String freshLabel() { 
		return "label" + (labCount++);
	} 

	public static String freshFunLabel() { 
		return "function" + (funLabCount++);
	} 
	
	public static String freshClassLabel() { 
		return "class" + (classLabCount++);
	} 
	
	public static String freshMethLabel() { 
		return "method" + (methLabCount++);
	} 
	
	public static void putCode(final String c) { 
		funCode += "\n" + c; //aggiunge una linea vuota di separazione prima di una funzione
	} 
	
	public static String getCode() { 
		return funCode;
	} 
	
}