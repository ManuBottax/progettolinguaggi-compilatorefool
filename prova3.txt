let
  var x:bool = 5>=3;
  
  fun f:bool (m:(int) -> bool)
    let 
      var x:bool = true;
    in 
       m(9);
    
  fun h: bool ( n: int) 
  	let
       fun t: bool (r : int)
		  let
		  in
			true;
  	   var i: int = 2;
  	in
  	   f(t);  
  	   
in  
  print ( 
   if f(h) 
     then { 12 }
     else { 10 }
  ) 
;