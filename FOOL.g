grammar FOOL;

@header{
import java.util.ArrayList;
import java.util.HashMap;
import ast.*;
import ast.interfaces.*;
import ast.basicNode.*;
import ast.program.*;
import ast.basicOperator.*;
import ast.type.*;
import entry.*;
import lib.*;
}

@lexer::members {
int lexicalErrors=0;
}

@members{
private int nestingLevel = -1;
private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();
private HashMap<String,CTentry> classTable = new HashMap<String,CTentry>();
//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
//il "fronte" della lista di tabelle � symTable.get(nestingLevel)
}

/*------------------------------------------------------------------
 * GRAMMAR (PARSER) RULES
 *------------------------------------------------------------------*/
  
prog	returns [Node ast]
	:       e=exp SEMIC	{$ast = new ProgNode($e.ast);}
        | LET 
            {nestingLevel++;
             HashMap<String,STentry> hm = new HashMap<String,STentry> ();
             symTable.add(hm);
            }
            c=cllist
            d=declist IN e=exp SEMIC 
            {symTable.remove(nestingLevel--);
             $ast = new ProgLetInNode($c.astlist, $d.astlist,$e.ast) ;} 
	;
	
	

	
cllist returns [ArrayList<ClassNode> astlist] : {
    $astlist= new ArrayList<ClassNode>(); 
    int classOffset=-2;
    HashMap<String, String> superTypes = new HashMap<String, String>(); 
    FOOLlib.setSuperType(superTypes);
}

( CLASS i=ID {  
                HashMap<String,STentry> hm = symTable.get(nestingLevel); 
                hm.put($i.text,new STentry(nestingLevel,null,classOffset));
                HashMap<String,STentry> vTable = new HashMap<String,STentry>();
                nestingLevel++;
                symTable.add(nestingLevel, vTable);
                ClassNode c = new ClassNode($i.text);
                CTentry entry = new CTentry(vTable);
                c.addCTentry(entry);
                if(classTable.put($i.text, entry) != null) {
                   System.out.println("Class "+$i.text+" at line "+$i.line+" already declared");
                   System.exit(0); 
                }
                $astlist.add(c);      
             }
(EXTENDS i2 = ID {  
                    if (!classTable.containsKey($i2.text)){
                      System.out.println("Class "+$i2.text+" at line "+$i2.line+" not exist (Cannot Extend from it)");
                      System.exit(0); 
                    }
                    //entry.setSuperClass(classTable.get($i2.text).copy());
                    c.setSuperCTentry(classTable.get($i2.text));
                    superTypes.put($i.text,$i2.text);
                 } 
              )?
   LPAR (p=ID COLON b=basic {  
//                            if($b.ast instanceof ArrowTypeNode) {
//                              System.out.println("field id "+$p.text+" at line "+$p.line+" can not be a function");
//                              System.exit(0);
//                            }
                            ArrayList<Node> fieldTypes = new ArrayList<Node>();
		                        int fieldoffset=-1;
		                        fieldTypes.add($b.ast); 
		                        FieldNode f = new FieldNode($p.text,$b.ast);
//		                        if ( vTable.put($p.text,new STentry(nestingLevel,$b.ast,fieldoffset--)) != null  ){
		                          //System.out.println("field id "+$p.text+" at line "+$p.line+" already declared");
		                          //System.exit(0);
//		                        }
		                        //System.out.println(f);
                            c.addField(f);
		                        entry.addField(f);
                         }
     (
        COMMA p2=ID COLON b2=basic {
                            fieldTypes.add($b2.ast); 
                            FieldNode f2 = new FieldNode($p2.text,$b2.ast);
                            c.addField(f2);
//                            if ( vTable.put($p2.text,new STentry(nestingLevel,$b2.ast,fieldoffset--)) != null  ){
//                              System.out.println("field id "+$p2.text+" at line "+$p2.line+" already declared");
//                              System.exit(0);
//                            }  
                            entry.addField(f2);
                       }
      )* )? RPAR    
              CLPAR{ int methodoffset = 0;}
                 ( /*FUN m=ID COLON basic LPAR (mp=ID COLON t=type (COMMA mp2=ID COLON t2=type)* )? RPAR
                       (LET (VAR ID COLON basic ASS exp SEMIC)* IN)? exp 
                   SEMIC*/
                 FUN m=ID COLON t=basic {
                        
			               MethodNode f = new MethodNode($m.text,$t.ast);
			               c.addMethod(f);
//			               vTable.put($m.text, methodEntry);
//			               if ( vTable.put($m.text, methodEntry) != null ) {
//			                  System.out.println("Method id "+$i.text+" at line "+$i.line+" already declared");
//			                  System.exit(0);
//			               }
			               STentry methodEntry = entry.addMethod(f);  
			               nestingLevel++;
			               HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
			               // hmn rappresenta la symtable all'interno del metodo f
			               symTable.add(nestingLevel, hmn);
		             }
              
              LPAR {
                 ArrayList<Node> parTypes = new ArrayList<Node>();
                 int paroffset=1;
              } 
                    // un solo parametro
                ( 
                  fid=ID COLON fty=type { 
	                  parTypes.add($fty.ast); //
	                  ParNode fpar = new ParNode($fid.text,$fty.ast);
	                  f.addPar(fpar);
	                  if ($fty.ast instanceof ArrowTypeNode) paroffset++;  // se ho un parametro funzionale devo tenere libero un altro spazio (aumento di 1 l'offset) 
	                  if ( hmn.put($fid.text,new STentry(nestingLevel,$fty.ast,paroffset++)) != null  )
	                    {System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
	                     System.exit(0);
	                  }
	                }
                  // altri parametri
                  (COMMA id=ID COLON ty=type
                    {
                    parTypes.add($ty.ast); //
                    ParNode par = new ParNode($id.text,$ty.ast);
                    f.addPar(par);
                    if ($ty.ast instanceof ArrowTypeNode) paroffset++; 
                    if ( hmn.put($id.text,new STentry(nestingLevel,$ty.ast,paroffset++)) != null  )
                      {System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
                       System.exit(0);}
                    }
                  )*
                )? {methodEntry.addType(new ArrowTypeNode(parTypes, $t.ast));
                
                }
                
                
              RPAR //{ entry.addType( new ArrowTypeNode(parTypes, $t.ast) );} //
                (LET d=declist IN{f.addDec($d.astlist);})? e=exp  {//chiudere scope
		              symTable.remove(nestingLevel--);
		              f.addBody($e.ast);
		            }
              SEMIC
                 )*                
              CRPAR {
                  symTable.remove(nestingLevel--);
              }
          )*
        ; 
        
// ------------------------------------------------------------------------------------------
declist returns [ArrayList<DecNode> astlist] : {           
                $astlist= new ArrayList<DecNode>();
                int offset = -2;/*
                if(nestingLevel==0) {
                    offset=globalOffset;
                }
                else {
                    offset = -2;
                } */
            }
        ( (
            VAR i=ID COLON t=type ASS e=exp{
              VarNode v = new VarNode($i.text,$t.ast,$e.ast); 
               $astlist.add(v); 
               HashMap<String,STentry> hm = symTable.get(nestingLevel);
               if ( hm.put($i.text,new STentry(nestingLevel,$t.ast,offset--)) != null  ) 
                 {System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
                  System.exit(0);} 
               if($t.ast instanceof ArrowTypeNode) offset--;
               if($t.ast instanceof ClassTypeNode) { 
                   if($e.ast instanceof NewNode) {
                        String classTypeID = ((ClassTypeNode)$t.ast).getId();
                        CTentry classType = classTable.get(classTypeID);
                        ((NewNode) $e.ast).setEffectiveType(classType);
                   }
               }
              }  
             
              
            | 
            FUN i=ID COLON t=basic
              {//inserimento di ID nella symtable     
               FunNode f = new FunNode($i.text,$t.ast);
               $astlist.add(f);
               HashMap<String,STentry> hm = symTable.get(nestingLevel);
               STentry entry = new STentry(nestingLevel,offset--); //separo introducendo "entry" 
               offset--;
               if ( hm.put($i.text,entry) != null )
                 {System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared");
                  System.exit(0);}
                  //creare una nuova hashmap per la symTable
               nestingLevel++;
               HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
               symTable.add(hmn);               
              }
              
              LPAR {ArrayList<Node> parTypes = new ArrayList<Node>();
                    int paroffset=1;} 
                    // un solo parametro
                (fid=ID COLON fty=type
                  { 
                  parTypes.add($fty.ast); //
                  ParNode fpar = new ParNode($fid.text,$fty.ast);
                  f.addPar(fpar);
                  if ($fty.ast instanceof ArrowTypeNode) paroffset++;  // se ho un parametro funzionale devo tenere libero un altro spazio (aumento di 1 l'offset) 
                  if ( hmn.put($fid.text,new STentry(nestingLevel,$fty.ast,paroffset++)) != null  )
                    {System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
                     System.exit(0);}
                  }
                  // altri parametri
                  (COMMA id=ID COLON ty=type
                    {
                    parTypes.add($ty.ast); //
                    ParNode par = new ParNode($id.text,$ty.ast);
                    f.addPar(par);
                    if ($ty.ast instanceof ArrowTypeNode) paroffset++; 
                    if ( hmn.put($id.text,new STentry(nestingLevel,$ty.ast,paroffset++)) != null  )
                      {System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
                       System.exit(0);}                       
                    }
                  )*
                )? 
              RPAR {entry.addType( new ArrowTypeNode(parTypes, $t.ast) );} //
              (LET d=declist IN{f.addDec($d.astlist);})? e=exp 
              {//chiudere scope
              symTable.remove(nestingLevel--);              
              f.addBody($e.ast);
              }

          ) SEMIC               
        )*          
  ;

	 
exp returns [Node ast]
  :         t=term {$ast=$t.ast;}
    (
      PLUS  p=term {$ast = new PlusNode ($ast, $p.ast);} 
    | MINUS m=term {$ast = new MinusNode ($ast, $m.ast);}
    | OR    o=term {$ast = new OrNode ($ast, $o.ast);}   
    )* 
  ;
  
term returns [Node ast]
  :           f=factor {$ast= $f.ast;} 
    (   TIMES t=factor {$ast= new MultNode ($ast,$t.ast);} 
      | DIV   d=factor {$ast= new DivNode ($ast,$d.ast);} 
      | AND   a=factor {$ast= new AndNode ($ast,$a.ast);} 
    )*
  ;
	
factor  returns [Node ast]
  :      v=value {$ast= $v.ast;}
    ( EQ e=value {$ast= new EqualNode ($v.ast,$e.ast);} 
    | GE g=value {$ast= new GreaterEqualNode ($v.ast,$g.ast);} 
    | LE l=value {$ast= new LowerEqualNode ($v.ast,$l.ast);} 
    )*
  ; 
 	
value returns [Node ast]
  :  n=INTEGER   {$ast= new IntNode(Integer.parseInt($n.text));}  
  
  | TRUE    {$ast= new BoolNode(true);}  
  
  | FALSE   {$ast= new BoolNode(false);}         
  
  | NULL    {$ast= new EmptyNode();}     
 
  | NEW i=ID { 
               CTentry entry = classTable.get($i.text);
               if (entry == null){
                  System.out.println("Class "+$i.text+" at line "+$i.line+" not declared");
                  System.exit(0);
               } 
               NewNode nn = new NewNode(entry, $i.text); 
              }
               
    LPAR (e1=exp {
                   
                  nn.addParameter($e1.ast);
                  } 
    (COMMA e2=exp {
                  
                  nn.addParameter($e2.ast);
                  }
    )* )? { $ast = nn;}
    
  
  RPAR         
  
  | IF x=exp 
    THEN CLPAR y=exp CRPAR 
    ELSE CLPAR z=exp CRPAR 
    {$ast= new IfNode($x.ast,$y.ast,$z.ast);} 
        
  | NOT LPAR e=exp {$ast= new NotNode($e.ast);} RPAR 
  
  | PRINT LPAR e=exp RPAR  {$ast= new PrintNode($e.ast);} 
         
  | LPAR e=exp RPAR {$ast= $e.ast;}   
       
  | i=ID { //try to find the definition of that identifier
			int j=nestingLevel;
			STentry entryID=null; 
			while (j>=0 && entryID==null) {
			    entryID=(symTable.get(j--)).get($i.text);
			}
			if (entryID==null) {
			    System.out.println("Id "+$i.text+" at line "+$i.line+" not declared");
			    System.exit(0);
			}               
			$ast= new IdNode($i.text,entryID, nestingLevel);
   }
   ( 
        LPAR {
            //qua verificare se e' possibile cosi' la chiamata di metodi
            ArrayList<Node> argList = new ArrayList<Node>();
          }
	        ( fa=exp {
	            argList.add($fa.ast);
	          }
	          (COMMA aa=exp {argList.add($aa.ast);})* 
	        )? 
        RPAR {$ast=new CallNode($i.text,entryID,argList, nestingLevel);}
        
        | DOT m=ID {/*qua suppongo ci vada una ricerca tipo quella sopra per vedere se il metodo � definito e non ho voglia di farla adesso*/
              if (! (entryID.getType() instanceof ClassTypeNode)) {
                System.out.println("id " + $i.text + " is not a class ");
                System.exit(0);
              }
              
              CTentry ct = classTable.get(((ClassTypeNode) entryID.getType()).getId());
        		  STentry methodEntryID = ct.getVTable().get($m.text);
        		  if(methodEntryID == null) {
        		    System.out.println("Method " + $m.text + " is not defined in class " + $i.text);
        		    System.exit(0);
        		  }
          } 
          LPAR {ArrayList<Node> argList = new ArrayList<Node>();}
	          (ma=exp {argList.add($ma.ast);} 
	            (COMMA aa=exp {argList.add($aa.ast);})* 
	          )? 
          RPAR {$ast=new ClassCallNode($i.text,$m.text,entryID,methodEntryID,argList, nestingLevel);} //non so se serva un nodo diverso o meno, intanto lo creo poi al massimo lo tolgo
  )?     
  ; 
 	
 	
type returns [Node ast]
  :   b=basic {$ast = $b.ast;}
    | a=arrow {$ast = $a.ast;}
  ;

basic returns [Node ast]
  :   INT   {$ast=new IntTypeNode();}           
    | BOOL  {$ast=new BoolTypeNode();}            
    | i = ID    {$ast=new ClassTypeNode($i.text);}                    
  ;  

arrow returns [Node ast]
  : LPAR {List<Node> parList = new ArrayList<Node>();}
  ( t1=type {parList.add($t1.ast);} ( COMMA t2=type {parList.add($t2.ast);})* )? RPAR ARROW b=basic {Node ret = $b.ast;} {$ast = new ArrowTypeNode(parList, ret);};
  	
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PLUS    : '+' ;
MINUS   : '-' ;
TIMES    : '*' ;
DIV   : '/' ;
LPAR  : '(' ;
RPAR  : ')' ;
CLPAR : '{' ;
CRPAR : '}' ;
SEMIC   : ';' ;
COLON   : ':' ; 
COMMA : ',' ;
DOT : '.' ;
OR  : '||';
AND : '&&';
NOT : 'not' ;
GE  : '>=' ;
LE  : '<=' ;
EQ  : '==' ;  
ASS : '=' ;
TRUE  : 'true' ;
FALSE : 'false' ;
IF  : 'if' ;
THEN  : 'then';
ELSE  : 'else' ;
PRINT : 'print' ;
LET     : 'let' ; 
IN      : 'in' ;  
VAR     : 'var' ;
FUN : 'fun' ; 
CLASS : 'class' ; 
EXTENDS : 'extends' ; 
NEW   : 'new' ; 
NULL    : 'null' ;    
INT : 'int' ;
BOOL  : 'bool' ;
ARROW   : '->' ;  
INTEGER : '0' | ('-')?(('1'..'9')('0'..'9')*) ; 

ID    : ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;

WHITESP : ( '\t' | ' ' | '\r' | '\n' )+    { $channel=HIDDEN; } ;

COMMENT : '/*' .* '*/' { $channel=HIDDEN; } ;
 
ERR      : . { System.out.println("Invalid char: "+$text); lexicalErrors++; $channel=HIDDEN; } ;

